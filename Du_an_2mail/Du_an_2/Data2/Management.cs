﻿using Data2.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2
{
   public class Management :DbContext
    {
        public Management():base("NuocHoaDbContext")
        {
          //  Database.SetInitializer(new MigrateDatabaseToLatestVersion<Management, Data2.Migrations.Configuration>("NuocHoaDbContext"));
        }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<Danh_muc> Danh_Mucs { get; set; }
        public DbSet<Hoa_don> Hoa_Dons { get; set; }
        public DbSet<Hoa_don_ct> Hoa_Don_Cts { get; set; }
        public DbSet<Khach_hang> Khach_Hangs { get; set; }
        public DbSet<San_pham> San_Phams { get; set; }
        public DbSet<Tin_tuc> Tin_Tucs { get; set; }
        public DbSet<Pt_thanh_toan> pt_Thanh_Toans { get; set; }
        public DbSet<Pt_van_chuyen> pt_Van_Chuyens { get; set; }
        public DbSet<Phan_hoi_kh> phan_Hoi_Khs { get; set; }
        public DbSet<Nha_cung_cap> nha_Cung_Caps { get; set; }
    }
}
