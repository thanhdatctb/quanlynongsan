﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity Find(object id);

        void Add(TEntity entity);

        void Edit(TEntity entity);

        void Delete(object id);

        void Save();
    
    }
}
