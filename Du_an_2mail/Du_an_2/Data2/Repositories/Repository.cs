﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Repositories
{

    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected Management _Context;
        protected DbSet<TEntity> _Table;
        public Repository()
        {
            _Context = new Management();
            _Table = _Context.Set<TEntity>();
        }
        public virtual void Add(TEntity entity)
        {
            _Table.Add(entity);
            Save();
        }

        public virtual void Delete(object id)
        {
            _Table.Remove(Find(id));
            Save();
        }

        public virtual void Edit(TEntity entity)
        {
            _Context.Entry(entity).State = EntityState.Modified;
            Save();
        }

        public virtual TEntity Find(object id)
        {
            return _Table.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _Table.AsEnumerable();
        }

        public void Save()
        {
            _Context.SaveChanges();
        }
    }

}
