﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
   public class Banner
    {
        [Key]
        public int Ma_banner { get; set; }
        [Display(Name = "Tên banner")]
        [Required(ErrorMessage ="{0} không được để trống")]
        public string Ten_banner { get; set; }
        [Display(Name = "ảnh kèm theo")]
        public string Anh_banner { get; set; }
        [Display(Name = "Đường link")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Link { get; set; }
        [Display(Name = "Trạng thái hiển thị")]

        public bool Trang_thai { get; set; }
        
    }
}
