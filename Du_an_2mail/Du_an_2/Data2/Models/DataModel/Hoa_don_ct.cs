﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Hoa_don_ct
    {
        [Key]
        public int Ma_hoa_don_ct{ get; set; }
        [ForeignKey ("Hoa_don")]
        public int Ma_hoa_don { get; set; }
        [ForeignKey("San_Pham")]
        public int Ma_san_pham { get; set; }
        [Display(Name = "Số lương")]
        public int So_luong { get; set; }
        [Display(Name = "Giá")]
        public float Gia { get; set; }
        [Display(Name = "Giá khuyến mãi")]
        public float Gia_km { get; set; }
        public virtual Hoa_don Hoa_don { get; set; }
        public virtual San_pham San_Pham { get; set; }
    }
}
