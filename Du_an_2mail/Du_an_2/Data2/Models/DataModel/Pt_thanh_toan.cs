﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{ 
    public class Pt_thanh_toan
    {
        [Key]
        public int Ma_pt_thanh_toan { get; set; }
        [Display(Name = "tên phương thức thanh toán")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ten_thanh_toan { get; set; }
        [Display(Name = "Trạng thái hiển thị")]
        
        public bool Trang_thai { get; set; }
    }
}
