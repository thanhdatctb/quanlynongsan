﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Tin_tuc
    {
        [Key]
        public int Ma_tin_tuc { get; set; }
        [Display(Name = "Tên tin tức")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ten_tin_tuc { get; set; }
        [Display(Name = "Nội dung tin")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Noi_dung { get; set; }
        [Display(Name = "ảnh kèm theo")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Anh { get; set; }
        [Display(Name = "Trạng thái hiển thị")]
        public bool Trang_thai { get; set; }
    }
}
