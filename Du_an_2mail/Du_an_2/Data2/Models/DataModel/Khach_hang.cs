﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Khach_hang
    {
        [Key]
        public int Ma_khach_hang  { get; set; }
        [Display(Name = "Họ và tên")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ho_ten { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Email { get; set; }

        [Display(Name = "Giới tính")]
        public bool Gioi_tinh { get; set; }
        [Display(Name = "Ngày sinh")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public DateTime Ngay_sinh { get; set; }
        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public int Dien_thoai { get; set; }
        [Display(Name = "Địa chỉ")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Dia_chi { get; set; }
        [Display(Name = "Tên đăng nhập")]
        
        public string User { get; set; }
        [Display(Name = "Mật khẩu")]
        
        public string Passwood { get; set; }
        [Display(Name = "Ngày tạo")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public DateTime Ngay_tao { get; set; }
        public virtual ICollection<Hoa_don>  Hoa_don{ get; set; }
        public  virtual ICollection<Phan_hoi_kh> Phan_Hoi_Khs { get; set; }

    }
}
