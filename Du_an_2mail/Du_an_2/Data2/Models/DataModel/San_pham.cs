﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class San_pham
    {
        [Key]
        public int Ma_san_pham { get; set; }
        [Display(Name = "Tên sản phẩm")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ten_san_pham { get; set; }
        [ForeignKey("Danh_muc")]
        [Display(Name = "Danh mục")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public int Ma_danh_muc { get; set; }
        [ForeignKey("Nha_Cung_Cap")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [Display(Name = "Nhà cung cấp")]
        public int Ma_nha_cung_cap { get; set; }
   
        [Display(Name = "Anh sản phẩm")]
        public string Anh_san_pham { get; set; }
        [Display(Name = "Mô tả")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Mo_ta { get; set; }
        [Display(Name = "Giá")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public float Gia { get; set; }
        [Display(Name = "Giá khuyến mãi")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public float Gia_km { get; set; }
        [Display(Name = "Ngày tạo")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public DateTime Ngay_tao { get; set; }
        [Display(Name = "Trạng thái hiển thị")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public bool Trang_thai_1 { get; set; }
        public bool Trang_thai_2 { get; set; }
        public virtual Danh_muc Danh_muc { get; set; }
        public virtual Nha_cung_cap Nha_Cung_Cap { get; set; }
        public virtual ICollection<Hoa_don_ct> Hoa_Don_Ct { get; set; }

    }
}
