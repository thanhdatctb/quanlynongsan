﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Phan_hoi_kh
    {
        [Key]
        public int Ma_phan_hoi { get; set; }
        [ForeignKey("Khach_Hang")]
        public int Ma_khach_hang { get; set; }
        [Display(Name = "Nội dung")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public String Noi_dung { get; set; }
        [Display(Name = "Ngày  tạo")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public DateTime Ngay_gio { get; set; }
        [Display(Name = "Trạng thái")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public bool Trang_thai { get; set; }
        public virtual Khach_hang  Khach_Hang { get; set; }

    }
}
