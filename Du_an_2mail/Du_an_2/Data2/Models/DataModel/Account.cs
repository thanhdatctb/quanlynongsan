﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Account
    {
        [Key]
        public int Ma_Account { get; set; }
        public string Ten { get; set; }
        public string Ten_dang_nhap { get; set; }
        public string Mat_khau { get; set; }
        public int Quyen_Han { get; set; }
    }
}
