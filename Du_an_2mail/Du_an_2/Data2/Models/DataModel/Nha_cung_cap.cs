﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Nha_cung_cap
    {
        [Key]
        public int Ma_nha_cung_cap { get; set; }
        [Display(Name = "Tên nhà cung cấp")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string  Ten_nha_cung_cap { get; set; }
        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public int So_dien_thoai { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Email { get; set; }
        [Display(Name = "Địa chỉ")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Dia_chi { get; set; }
        [Display(Name = "Hiển thị")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public bool Trang_thai_1 { get; set; }
        public virtual ICollection<San_pham> San_Phams { get; set; }
    }
}
