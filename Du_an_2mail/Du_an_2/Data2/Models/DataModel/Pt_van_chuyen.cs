﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
   public  class Pt_van_chuyen
    {
        [Key]
        public int Ma_van_chuyen { get; set; }
        [Display(Name = "Tên phương thức vận chuyển")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ten_van_chuyen { get; set; }
        [Display(Name = "Trạng thái hiển thị")]
        public bool Trang_thai { get; set; }
    }
}
