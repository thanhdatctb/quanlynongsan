﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Hoa_don
    {
        [Key]
        public int Ma_hoa_don { get; set; }
        [ForeignKey("Khach_hang")]
        public int Ma_khach_hang { get; set; }
        [Display(Name = "Tổng tiền")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public float Tong_tien { get; set; }
        [Display(Name = "Ngày tạo")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public DateTime Ngay_tao { get; set; }
        
        [Display(Name = "Tên khách hàng")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ten_khach_hang { get; set; }
        [Display(Name = "Email khách hàng")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Emai_khach_hang { get; set; }
        [Display(Name = "Số điện thoại khách hàng")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public int So_dien_thoai { get; set; }
        [Display(Name = "Địa chỉ")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Dia_chi { get; set; }
        [Display(Name = "Ghi chú")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ghi_chu { get; set; }
        [Display(Name = "Trạng thái")]
        public bool Trang_thai { get; set; }
        [ForeignKey("Pt_Van_Chuyen")]
        public int Ma_ptvan_chuyen { get; set; }
        [ForeignKey("Pt_Thanh_Toan")]
        public int Ma_ptthanh_toan { get; set; }
        public bool Trang_thai_1 { get; set; }
        public virtual ICollection<Hoa_don_ct> Hoa_Don_Ct { get; set; }
        public virtual Khach_hang Khach_hang { get; set; }
        public virtual Pt_thanh_toan Pt_Thanh_Toan { get; set; }
        public virtual Pt_van_chuyen Pt_Van_Chuyen { get; set; }

    }
}
