﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data2.Models.DataModel
{
    public class Danh_muc
    {
        [Key]
        public int Ma_danh_muc { get; set; }
        [Display(Name ="Tên danh mục")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Ten_danh_muc { get; set; }
        [Display(Name = "Hiển thị cho người dùng")]
        public bool Trang_thai_1 { get; set; }
        [Display(Name = "Hiển thị cho quản trị")]
        public bool Trang_thai_2 { get; set; }
        public virtual ICollection<San_pham> san_Phams { get; set; }

    }
}
