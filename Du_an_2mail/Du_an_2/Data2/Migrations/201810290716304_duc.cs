namespace Data2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class duc : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Ma_Account = c.Int(nullable: false, identity: true),
                        Ten = c.String(),
                        Ten_dang_nhap = c.String(),
                        Mat_khau = c.String(),
                        Quyen_Han = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_Account);
            
            CreateTable(
                "dbo.Banners",
                c => new
                    {
                        Ma_banner = c.Int(nullable: false, identity: true),
                        Ten_banner = c.String(nullable: false),
                        Anh_banner = c.String(nullable: false),
                        Link = c.String(nullable: false),
                        Trang_thai = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_banner);
            
            CreateTable(
                "dbo.Danh_muc",
                c => new
                    {
                        Ma_danh_muc = c.Int(nullable: false, identity: true),
                        Ten_danh_muc = c.String(nullable: false),
                        Trang_thai_1 = c.Boolean(nullable: false),
                        Trang_thai_2 = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_danh_muc);
            
            CreateTable(
                "dbo.San_pham",
                c => new
                    {
                        Ma_san_pham = c.Int(nullable: false, identity: true),
                        Ten_san_pham = c.String(nullable: false),
                        Ma_danh_muc = c.Int(nullable: false),
                        Ma_nha_cung_cap = c.Int(nullable: false),
                        Anh_san_pham = c.String(),
                        Mo_ta = c.String(nullable: false),
                        Gia = c.Single(nullable: false),
                        Gia_km = c.Single(nullable: false),
                        Ngay_tao = c.DateTime(nullable: false),
                        Trang_thai_1 = c.Boolean(nullable: false),
                        Trang_thai_2 = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_san_pham)
                .ForeignKey("dbo.Danh_muc", t => t.Ma_danh_muc, cascadeDelete: true)
                .ForeignKey("dbo.Nha_cung_cap", t => t.Ma_nha_cung_cap, cascadeDelete: true)
                .Index(t => t.Ma_danh_muc)
                .Index(t => t.Ma_nha_cung_cap);
            
            CreateTable(
                "dbo.Hoa_don_ct",
                c => new
                    {
                        Ma_hoa_don_ct = c.Int(nullable: false, identity: true),
                        Ma_hoa_don = c.Int(nullable: false),
                        Ma_san_pham = c.Int(nullable: false),
                        So_luong = c.Int(nullable: false),
                        Gia = c.Single(nullable: false),
                        Gia_km = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_hoa_don_ct)
                .ForeignKey("dbo.Hoa_don", t => t.Ma_hoa_don, cascadeDelete: true)
                .ForeignKey("dbo.San_pham", t => t.Ma_san_pham, cascadeDelete: true)
                .Index(t => t.Ma_hoa_don)
                .Index(t => t.Ma_san_pham);
            
            CreateTable(
                "dbo.Hoa_don",
                c => new
                    {
                        Ma_hoa_don = c.Int(nullable: false, identity: true),
                        Ma_khach_hang = c.Int(nullable: false),
                        Tong_tien = c.Single(nullable: false),
                        Ngay_tao = c.DateTime(nullable: false),
                        Ten_khach_hang = c.String(nullable: false),
                        Emai_khach_hang = c.String(nullable: false),
                        So_dien_thoai = c.Int(nullable: false),
                        Dia_chi = c.String(nullable: false),
                        Ghi_chu = c.String(nullable: false),
                        Trang_thai = c.Boolean(nullable: false),
                        Ma_ptvan_chuyen = c.Int(nullable: false),
                        Ma_ptthanh_toan = c.Int(nullable: false),
                        Trang_thai_1 = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_hoa_don)
                .ForeignKey("dbo.Khach_hang", t => t.Ma_khach_hang, cascadeDelete: true)
                .ForeignKey("dbo.Pt_thanh_toan", t => t.Ma_ptthanh_toan, cascadeDelete: true)
                .ForeignKey("dbo.Pt_van_chuyen", t => t.Ma_ptvan_chuyen, cascadeDelete: true)
                .Index(t => t.Ma_khach_hang)
                .Index(t => t.Ma_ptvan_chuyen)
                .Index(t => t.Ma_ptthanh_toan);
            
            CreateTable(
                "dbo.Khach_hang",
                c => new
                    {
                        Ma_khach_hang = c.Int(nullable: false, identity: true),
                        Ho_ten = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Gioi_tinh = c.Boolean(nullable: false),
                        Ngay_sinh = c.DateTime(nullable: false),
                        Dien_thoai = c.Int(nullable: false),
                        Dia_chi = c.String(nullable: false),
                        User = c.String(),
                        Passwood = c.String(),
                        Ngay_tao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_khach_hang);
            
            CreateTable(
                "dbo.Phan_hoi_kh",
                c => new
                    {
                        Ma_phan_hoi = c.Int(nullable: false, identity: true),
                        Ma_khach_hang = c.Int(nullable: false),
                        Noi_dung = c.String(nullable: false),
                        Ngay_gio = c.DateTime(nullable: false),
                        Trang_thai = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_phan_hoi)
                .ForeignKey("dbo.Khach_hang", t => t.Ma_khach_hang, cascadeDelete: true)
                .Index(t => t.Ma_khach_hang);
            
            CreateTable(
                "dbo.Pt_thanh_toan",
                c => new
                    {
                        Ma_pt_thanh_toan = c.Int(nullable: false, identity: true),
                        Ten_thanh_toan = c.String(nullable: false),
                        Trang_thai = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_pt_thanh_toan);
            
            CreateTable(
                "dbo.Pt_van_chuyen",
                c => new
                    {
                        Ma_van_chuyen = c.Int(nullable: false, identity: true),
                        Ten_van_chuyen = c.String(nullable: false),
                        Trang_thai = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_van_chuyen);
            
            CreateTable(
                "dbo.Nha_cung_cap",
                c => new
                    {
                        Ma_nha_cung_cap = c.Int(nullable: false, identity: true),
                        Ten_nha_cung_cap = c.String(nullable: false),
                        So_dien_thoai = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        Dia_chi = c.String(nullable: false),
                        Trang_thai_1 = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_nha_cung_cap);
            
            CreateTable(
                "dbo.Tin_tuc",
                c => new
                    {
                        Ma_tin_tuc = c.Int(nullable: false, identity: true),
                        Ten_tin_tuc = c.String(nullable: false),
                        Noi_dung = c.String(nullable: false),
                        Anh = c.String(nullable: false),
                        Trang_thai = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Ma_tin_tuc);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.San_pham", "Ma_nha_cung_cap", "dbo.Nha_cung_cap");
            DropForeignKey("dbo.Hoa_don_ct", "Ma_san_pham", "dbo.San_pham");
            DropForeignKey("dbo.Hoa_don_ct", "Ma_hoa_don", "dbo.Hoa_don");
            DropForeignKey("dbo.Hoa_don", "Ma_ptvan_chuyen", "dbo.Pt_van_chuyen");
            DropForeignKey("dbo.Hoa_don", "Ma_ptthanh_toan", "dbo.Pt_thanh_toan");
            DropForeignKey("dbo.Hoa_don", "Ma_khach_hang", "dbo.Khach_hang");
            DropForeignKey("dbo.Phan_hoi_kh", "Ma_khach_hang", "dbo.Khach_hang");
            DropForeignKey("dbo.San_pham", "Ma_danh_muc", "dbo.Danh_muc");
            DropIndex("dbo.Phan_hoi_kh", new[] { "Ma_khach_hang" });
            DropIndex("dbo.Hoa_don", new[] { "Ma_ptthanh_toan" });
            DropIndex("dbo.Hoa_don", new[] { "Ma_ptvan_chuyen" });
            DropIndex("dbo.Hoa_don", new[] { "Ma_khach_hang" });
            DropIndex("dbo.Hoa_don_ct", new[] { "Ma_san_pham" });
            DropIndex("dbo.Hoa_don_ct", new[] { "Ma_hoa_don" });
            DropIndex("dbo.San_pham", new[] { "Ma_nha_cung_cap" });
            DropIndex("dbo.San_pham", new[] { "Ma_danh_muc" });
            DropTable("dbo.Tin_tuc");
            DropTable("dbo.Nha_cung_cap");
            DropTable("dbo.Pt_van_chuyen");
            DropTable("dbo.Pt_thanh_toan");
            DropTable("dbo.Phan_hoi_kh");
            DropTable("dbo.Khach_hang");
            DropTable("dbo.Hoa_don");
            DropTable("dbo.Hoa_don_ct");
            DropTable("dbo.San_pham");
            DropTable("dbo.Danh_muc");
            DropTable("dbo.Banners");
            DropTable("dbo.Accounts");
        }
    }
}
