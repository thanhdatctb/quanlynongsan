namespace Data2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class duc1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Banners", "Anh_banner", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Banners", "Anh_banner", c => c.String(nullable: false));
        }
    }
}
