﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Data2.Repositories;

namespace Du_an_2.Areas.Admin.Controllers
{

    public class BaseController<TEntity> : Controller where TEntity : class
    {
        // GET: Admin/Base

        protected Repository<TEntity> _repository;
        public BaseController()
        {
            _repository = new Repository<TEntity>();
        }
        public virtual ActionResult GetData()
        {
            var data = _repository.GetAll();
            // xử lý chuỗi trước khi trả về
            var json = JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult DetaitData(int id)
        {
            var data = _repository.Find(id);
            // xử lý chuỗi trước khi trả về
            var json = JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            //trả về kết quả đc xử lý json
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        //PostFileBase dùng để sumbit dữ liệu khi from có file ảnh hoặc các file văn bản kèm theo
        public virtual string CreateData(TEntity entity, HttpPostedFileBase fileUpload)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Add(entity);
                    return "thành công";
                }
                catch (Exception)
                {

                    return " lỗi";
                }

            }
            return "Lỗi";
        }
        [HttpPost]
        //PostFileBase dùng để sumbit dữ liệu khi from có file ảnh hoặc các file văn bản kèm theo
        public virtual string UpdateData(TEntity entity, HttpPostedFileBase fileUpload)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Edit(entity);
                    return "thành công";
                }
                catch (Exception)
                {

                    return " lỗi";
                }

            }
            return "Lỗi";
        }
        public string DeleteData(object id)
        {
            try
            {
                _repository.Delete(id);
                return "thành công";
            }
            catch (Exception)
            {
                return "thất bại";
            }
        }
    }
}