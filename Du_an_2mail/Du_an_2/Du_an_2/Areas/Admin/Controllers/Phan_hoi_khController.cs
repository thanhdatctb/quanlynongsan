﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Phan_hoi_khController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Phan_hoi_kh
        public ActionResult Index()
        {
            var phan_Hoi_Khs = db.phan_Hoi_Khs.Include(p => p.Khach_Hang);
            return View(phan_Hoi_Khs.ToList());
        }

        // GET: Admin/Phan_hoi_kh/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phan_hoi_kh phan_hoi_kh = db.phan_Hoi_Khs.Find(id);
            if (phan_hoi_kh == null)
            {
                return HttpNotFound();
            }
            return View(phan_hoi_kh);
        }

        // GET: Admin/Phan_hoi_kh/Create
        public ActionResult Create()
        {
            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten");
            return View();
        }

        // POST: Admin/Phan_hoi_kh/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_phan_hoi,Ma_khach_hang,Noi_dung,Ngay_gio,Trang_thai")] Phan_hoi_kh phan_hoi_kh)
        {
            if (ModelState.IsValid)
            {
                db.phan_Hoi_Khs.Add(phan_hoi_kh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten", phan_hoi_kh.Ma_khach_hang);
            return View(phan_hoi_kh);
        }

        // GET: Admin/Phan_hoi_kh/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phan_hoi_kh phan_hoi_kh = db.phan_Hoi_Khs.Find(id);
            if (phan_hoi_kh == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten", phan_hoi_kh.Ma_khach_hang);
            return View(phan_hoi_kh);
        }

        // POST: Admin/Phan_hoi_kh/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_phan_hoi,Ma_khach_hang,Noi_dung,Ngay_gio,Trang_thai")] Phan_hoi_kh phan_hoi_kh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phan_hoi_kh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten", phan_hoi_kh.Ma_khach_hang);
            return View(phan_hoi_kh);
        }

        // GET: Admin/Phan_hoi_kh/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            } 
            Phan_hoi_kh phan_hoi_kh = db.phan_Hoi_Khs.Find(id);
            if (phan_hoi_kh == null)
            {
                return HttpNotFound();
            }
            return View(phan_hoi_kh);
        }

        // POST: Admin/Phan_hoi_kh/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Phan_hoi_kh phan_hoi_kh = db.phan_Hoi_Khs.Find(id);
            db.phan_Hoi_Khs.Remove(phan_hoi_kh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
