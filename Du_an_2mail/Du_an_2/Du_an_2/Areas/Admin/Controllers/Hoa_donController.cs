﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Hoa_donController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Hoa_don
        public ActionResult Index()
        {
            var hoa_Dons = db.Hoa_Dons.Include(h => h.Khach_hang).Include(h => h.Pt_Thanh_Toan).Include(h => h.Pt_Van_Chuyen);
            return View(hoa_Dons.ToList());
        }

        // GET: Admin/Hoa_don/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoa_don hoa_don = db.Hoa_Dons.Find(id);
            if (hoa_don == null)
            {
                return HttpNotFound();
            }
            return View(hoa_don);
        }

        // GET: Admin/Hoa_don/Create
        public ActionResult Create()
        {
            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten");
            ViewBag.Ma_ptthanh_toan = new SelectList(db.pt_Thanh_Toans, "Ma_pt_thanh_toan", "Ten_thanh_toan");
            ViewBag.Ma_ptvan_chuyen = new SelectList(db.pt_Van_Chuyens, "Ma_van_chuyen", "Ten_van_chuyen");
            return View();
        }

        // POST: Admin/Hoa_don/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_hoa_don,Ma_khach_hang,Tong_tien,Ngay_tao,Ten_khach_hang,Emai_khach_hang,So_dien_thoai,Dia_chi,Ghi_chu,Trang_thai,Ma_ptvan_chuyen,Ma_ptthanh_toan,Trang_thai_1")] Hoa_don hoa_don)
        {
            if (ModelState.IsValid)
            {
                db.Hoa_Dons.Add(hoa_don);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten", hoa_don.Ma_khach_hang);
            ViewBag.Ma_ptthanh_toan = new SelectList(db.pt_Thanh_Toans, "Ma_pt_thanh_toan", "Ten_thanh_toan", hoa_don.Ma_ptthanh_toan);
            ViewBag.Ma_ptvan_chuyen = new SelectList(db.pt_Van_Chuyens, "Ma_van_chuyen", "Ten_van_chuyen", hoa_don.Ma_ptvan_chuyen);
            return View(hoa_don);
        }

        // GET: Admin/Hoa_don/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoa_don hoa_don = db.Hoa_Dons.Find(id);
            if (hoa_don == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten", hoa_don.Ma_khach_hang);
            ViewBag.Ma_ptthanh_toan = new SelectList(db.pt_Thanh_Toans, "Ma_pt_thanh_toan", "Ten_thanh_toan", hoa_don.Ma_ptthanh_toan);
            ViewBag.Ma_ptvan_chuyen = new SelectList(db.pt_Van_Chuyens, "Ma_van_chuyen", "Ten_van_chuyen", hoa_don.Ma_ptvan_chuyen);
            return View(hoa_don);
        }

        // POST: Admin/Hoa_don/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_hoa_don,Ma_khach_hang,Tong_tien,Ngay_tao,Ten_khach_hang,Emai_khach_hang,So_dien_thoai,Dia_chi,Ghi_chu,Trang_thai,Ma_ptvan_chuyen,Ma_ptthanh_toan,Trang_thai_1")] Hoa_don hoa_don)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoa_don).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ma_khach_hang = new SelectList(db.Khach_Hangs, "Ma_khach_hang", "Ho_ten", hoa_don.Ma_khach_hang);
            ViewBag.Ma_ptthanh_toan = new SelectList(db.pt_Thanh_Toans, "Ma_pt_thanh_toan", "Ten_thanh_toan", hoa_don.Ma_ptthanh_toan);
            ViewBag.Ma_ptvan_chuyen = new SelectList(db.pt_Van_Chuyens, "Ma_van_chuyen", "Ten_van_chuyen", hoa_don.Ma_ptvan_chuyen);
            return View(hoa_don);
        }

        // GET: Admin/Hoa_don/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoa_don hoa_don = db.Hoa_Dons.Find(id);
            if (hoa_don == null)
            {
                return HttpNotFound();
            }
            return View(hoa_don);
        }

        // POST: Admin/Hoa_don/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hoa_don hoa_don = db.Hoa_Dons.Find(id);
            db.Hoa_Dons.Remove(hoa_don);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
