﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Nha_cung_capController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Nha_cung_cap
        public ActionResult Index()
        {
            return View(db.nha_Cung_Caps.ToList());
        }

        // GET: Admin/Nha_cung_cap/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nha_cung_cap nha_cung_cap = db.nha_Cung_Caps.Find(id);
            if (nha_cung_cap == null)
            {
                return HttpNotFound();
            }
            return View(nha_cung_cap);
        }

        // GET: Admin/Nha_cung_cap/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Nha_cung_cap/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_nha_cung_cap,Ten_nha_cung_cap,So_dien_thoai,Email,Dia_chi,Trang_thai_1")] Nha_cung_cap nha_cung_cap)
        {
            if (ModelState.IsValid)
            {
                db.nha_Cung_Caps.Add(nha_cung_cap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nha_cung_cap);
        }

        // GET: Admin/Nha_cung_cap/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nha_cung_cap nha_cung_cap = db.nha_Cung_Caps.Find(id);
            if (nha_cung_cap == null)
            {
                return HttpNotFound();
            }
            return View(nha_cung_cap);
        }

        // POST: Admin/Nha_cung_cap/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_nha_cung_cap,Ten_nha_cung_cap,So_dien_thoai,Email,Dia_chi,Trang_thai_1")] Nha_cung_cap nha_cung_cap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nha_cung_cap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nha_cung_cap);
        }

        // GET: Admin/Nha_cung_cap/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nha_cung_cap nha_cung_cap = db.nha_Cung_Caps.Find(id);
            if (nha_cung_cap == null)
            {
                return HttpNotFound();
            }
            return View(nha_cung_cap);
        }

        // POST: Admin/Nha_cung_cap/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Nha_cung_cap nha_cung_cap = db.nha_Cung_Caps.Find(id);
            db.nha_Cung_Caps.Remove(nha_cung_cap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
