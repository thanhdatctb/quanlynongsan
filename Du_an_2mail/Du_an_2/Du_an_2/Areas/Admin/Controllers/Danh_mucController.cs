﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Danh_mucController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Danh_muc
        public ActionResult Index()
        {
            return View(db.Danh_Mucs.ToList());
        }

        // GET: Admin/Danh_muc/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Danh_muc danh_muc = db.Danh_Mucs.Find(id);
            if (danh_muc == null)
            {
                return HttpNotFound();
            }
            return View(danh_muc);
        }

        // GET: Admin/Danh_muc/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Danh_muc/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_danh_muc,Ten_danh_muc,Trang_thai_1,Trang_thai_2")] Danh_muc danh_muc)
        {
            if (ModelState.IsValid)
            {
                db.Danh_Mucs.Add(danh_muc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(danh_muc);
        }

        // GET: Admin/Danh_muc/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Danh_muc danh_muc = db.Danh_Mucs.Find(id);
            if (danh_muc == null)
            {
                return HttpNotFound();
            }
            return View(danh_muc);
        }

        // POST: Admin/Danh_muc/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_danh_muc,Ten_danh_muc,Trang_thai_1,Trang_thai_2")] Danh_muc danh_muc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(danh_muc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(danh_muc);
        }

        // GET: Admin/Danh_muc/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Danh_muc danh_muc = db.Danh_Mucs.Find(id);
            if (danh_muc == null)
            {
                return HttpNotFound();
            }
            return View(danh_muc);
        }

        // POST: Admin/Danh_muc/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Danh_muc danh_muc = db.Danh_Mucs.Find(id);
            db.Danh_Mucs.Remove(danh_muc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
