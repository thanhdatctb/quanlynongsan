﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Hoa_don_ctController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Hoa_don_ct
        public ActionResult Index()
        {
            var hoa_Don_Cts = db.Hoa_Don_Cts.Include(h => h.Hoa_don).Include(h => h.San_Pham);
            return View(hoa_Don_Cts.ToList());
        }

        // GET: Admin/Hoa_don_ct/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoa_don_ct hoa_don_ct = db.Hoa_Don_Cts.Find(id);
            if (hoa_don_ct == null)
            {
                return HttpNotFound();
            }
            return View(hoa_don_ct);
        }

        // GET: Admin/Hoa_don_ct/Create
        public ActionResult Create()
        {
            ViewBag.Ma_hoa_don = new SelectList(db.Hoa_Dons, "Ma_hoa_don", "Ten_khach_hang");
            ViewBag.Ma_san_pham = new SelectList(db.San_Phams, "Ma_san_pham", "Ten_san_pham");
            return View();
        }

        // POST: Admin/Hoa_don_ct/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_hoa_don_ct,Ma_hoa_don,Ma_san_pham,So_luong,Gia,Gia_km")] Hoa_don_ct hoa_don_ct)
        {
            if (ModelState.IsValid)
            {
                db.Hoa_Don_Cts.Add(hoa_don_ct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Ma_hoa_don = new SelectList(db.Hoa_Dons, "Ma_hoa_don", "Ten_khach_hang", hoa_don_ct.Ma_hoa_don);
            ViewBag.Ma_san_pham = new SelectList(db.San_Phams, "Ma_san_pham", "Ten_san_pham", hoa_don_ct.Ma_san_pham);
            return View(hoa_don_ct);
        }

        // GET: Admin/Hoa_don_ct/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoa_don_ct hoa_don_ct = db.Hoa_Don_Cts.Find(id);
            if (hoa_don_ct == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ma_hoa_don = new SelectList(db.Hoa_Dons, "Ma_hoa_don", "Ten_khach_hang", hoa_don_ct.Ma_hoa_don);
            ViewBag.Ma_san_pham = new SelectList(db.San_Phams, "Ma_san_pham", "Ten_san_pham", hoa_don_ct.Ma_san_pham);
            return View(hoa_don_ct);
        }

        // POST: Admin/Hoa_don_ct/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_hoa_don_ct,Ma_hoa_don,Ma_san_pham,So_luong,Gia,Gia_km")] Hoa_don_ct hoa_don_ct)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoa_don_ct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ma_hoa_don = new SelectList(db.Hoa_Dons, "Ma_hoa_don", "Ten_khach_hang", hoa_don_ct.Ma_hoa_don);
            ViewBag.Ma_san_pham = new SelectList(db.San_Phams, "Ma_san_pham", "Ten_san_pham", hoa_don_ct.Ma_san_pham);
            return View(hoa_don_ct);
        }

        // GET: Admin/Hoa_don_ct/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoa_don_ct hoa_don_ct = db.Hoa_Don_Cts.Find(id);
            if (hoa_don_ct == null)
            {
                return HttpNotFound();
            }
            return View(hoa_don_ct);
        }

        // POST: Admin/Hoa_don_ct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hoa_don_ct hoa_don_ct = db.Hoa_Don_Cts.Find(id);
            db.Hoa_Don_Cts.Remove(hoa_don_ct);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
