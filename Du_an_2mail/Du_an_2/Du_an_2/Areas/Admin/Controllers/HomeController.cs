﻿using Data2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        Management db = new Management();
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            var user = db.Accounts.SingleOrDefault(x => x.Ten_dang_nhap == username && x.Mat_khau == password && x.Quyen_Han == 1);
            if (user != null)
            {
                Session["iduser"] = user.Ma_Account;
                Session["name"] = user.Ten_dang_nhap;
                Session["username"] = user.Ten_dang_nhap;
                Session["password"] = user.Mat_khau;
                return RedirectToAction("Index", "San_pham");
            }
            ViewBag.error = "Đăng nhập lỗi hoặc bạn không có quyền";
            return View();
        }
        public ActionResult Logout()
        {

            Session["iduser"] = null;
            Session["name"] = null;
            Session["username"] = null;
            Session["password"] = null;
            return RedirectToAction("Login", "Home");

        }
    }
}