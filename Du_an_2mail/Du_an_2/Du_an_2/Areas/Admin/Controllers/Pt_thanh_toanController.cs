﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Pt_thanh_toanController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Pt_thanh_toan
        public ActionResult Index()
        {
            return View(db.pt_Thanh_Toans.ToList());
        }

        // GET: Admin/Pt_thanh_toan/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pt_thanh_toan pt_thanh_toan = db.pt_Thanh_Toans.Find(id);
            if (pt_thanh_toan == null)
            {
                return HttpNotFound();
            }
            return View(pt_thanh_toan);
        }

        // GET: Admin/Pt_thanh_toan/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Pt_thanh_toan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_pt_thanh_toan,Ten_thanh_toan,Trang_thai")] Pt_thanh_toan pt_thanh_toan)
        {
            if (ModelState.IsValid)
            {
                db.pt_Thanh_Toans.Add(pt_thanh_toan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pt_thanh_toan);
        }

        // GET: Admin/Pt_thanh_toan/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pt_thanh_toan pt_thanh_toan = db.pt_Thanh_Toans.Find(id);
            if (pt_thanh_toan == null)
            {
                return HttpNotFound();
            }
            return View(pt_thanh_toan);
        }

        // POST: Admin/Pt_thanh_toan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_pt_thanh_toan,Ten_thanh_toan,Trang_thai")] Pt_thanh_toan pt_thanh_toan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pt_thanh_toan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pt_thanh_toan);
        }

        // GET: Admin/Pt_thanh_toan/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pt_thanh_toan pt_thanh_toan = db.pt_Thanh_Toans.Find(id);
            if (pt_thanh_toan == null)
            {
                return HttpNotFound();
            }
            return View(pt_thanh_toan);
        }

        // POST: Admin/Pt_thanh_toan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pt_thanh_toan pt_thanh_toan = db.pt_Thanh_Toans.Find(id);
            db.pt_Thanh_Toans.Remove(pt_thanh_toan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
