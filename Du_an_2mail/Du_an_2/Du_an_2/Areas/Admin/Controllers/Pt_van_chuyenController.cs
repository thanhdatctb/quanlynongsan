﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class Pt_van_chuyenController : Controller
    {
        private Management db = new Management();

        // GET: Admin/Pt_van_chuyen
        public ActionResult Index()
        {
            return View(db.pt_Van_Chuyens.ToList());
        }

        // GET: Admin/Pt_van_chuyen/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pt_van_chuyen pt_van_chuyen = db.pt_Van_Chuyens.Find(id);
            if (pt_van_chuyen == null)
            {
                return HttpNotFound();
            }
            return View(pt_van_chuyen);
        }

        // GET: Admin/Pt_van_chuyen/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Pt_van_chuyen/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_van_chuyen,Ten_van_chuyen,Trang_thai")] Pt_van_chuyen pt_van_chuyen)
        {
            if (ModelState.IsValid)
            {
                db.pt_Van_Chuyens.Add(pt_van_chuyen);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pt_van_chuyen);
        }

        // GET: Admin/Pt_van_chuyen/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pt_van_chuyen pt_van_chuyen = db.pt_Van_Chuyens.Find(id);
            if (pt_van_chuyen == null)
            {
                return HttpNotFound();
            }
            return View(pt_van_chuyen);
        }

        // POST: Admin/Pt_van_chuyen/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_van_chuyen,Ten_van_chuyen,Trang_thai")] Pt_van_chuyen pt_van_chuyen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pt_van_chuyen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pt_van_chuyen);
        }

        // GET: Admin/Pt_van_chuyen/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pt_van_chuyen pt_van_chuyen = db.pt_Van_Chuyens.Find(id);
            if (pt_van_chuyen == null)
            {
                return HttpNotFound();
            }
            return View(pt_van_chuyen);
        }

        // POST: Admin/Pt_van_chuyen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pt_van_chuyen pt_van_chuyen = db.pt_Van_Chuyens.Find(id);
            db.pt_Van_Chuyens.Remove(pt_van_chuyen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
