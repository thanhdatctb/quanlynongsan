﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;
using PagedList;

namespace Du_an_2.Areas.Admin.Controllers
{
    public class San_phamController : Controller
    {
        private Management db = new Management();

        // GET: Admin/San_pham
        public ActionResult Index(string searchString, FormCollection f, int? page)
        {
            ViewBag.KeyWord = searchString;
            List<San_pham> sp = db.San_Phams.Where(n => n.Ten_san_pham.Contains(searchString)).ToList();

            //Phan trang
            int pageNumber = (page ?? 1);
            int pageSize = 15;
            if (sp.Count == 0)
            {
                ViewBag.ThongBao = "Khong tim thay san pham nao";
                return View(db.San_Phams.OrderBy(n => n.Ten_san_pham).ToPagedList(pageNumber, pageSize));
            }
            return View(sp.OrderBy(n => n.Ten_san_pham).ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/San_pham/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            San_pham san_pham = db.San_Phams.Find(id);
            if (san_pham == null)
            {
                return HttpNotFound();
            }
            return View(san_pham);
        }

        // GET: Admin/San_pham/Create
        public ActionResult Create()
        {
            ViewBag.Ma_danh_muc = new SelectList(db.Danh_Mucs, "Ma_danh_muc", "Ten_danh_muc");
            ViewBag.Ma_nha_cung_cap = new SelectList(db.nha_Cung_Caps, "Ma_nha_cung_cap", "Ten_nha_cung_cap");
            return View();
        }

        // POST: Admin/San_pham/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_san_pham,Ten_san_pham,Ma_danh_muc,Ma_nha_cung_cap,Anh_san_pham,Mo_ta,Gia,Gia_km,Ngay_tao,Trang_thai_1,Trang_thai_2")] San_pham san_pham, HttpPostedFileBase PictureUpload)
        {
            if (ModelState.IsValid)
            {
                if (PictureUpload.FileName != null)
                {
                    PictureUpload.SaveAs(Server.MapPath("~/Areas/Admin/Content/image/") + PictureUpload.FileName);
                    san_pham.Anh_san_pham = "/Areas/Admin/Content/image/" + PictureUpload.FileName;
                }
                db.San_Phams.Add(san_pham);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Ma_danh_muc = new SelectList(db.Danh_Mucs, "Ma_danh_muc", "Ten_danh_muc", san_pham.Ma_danh_muc);
            ViewBag.Ma_nha_cung_cap = new SelectList(db.nha_Cung_Caps, "Ma_nha_cung_cap", "Ten_nha_cung_cap", san_pham.Ma_nha_cung_cap);
            return View(san_pham);
        }

        // GET: Admin/San_pham/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            San_pham san_pham = db.San_Phams.Find(id);
            if (san_pham == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ma_danh_muc = new SelectList(db.Danh_Mucs, "Ma_danh_muc", "Ten_danh_muc", san_pham.Ma_danh_muc);
            ViewBag.Ma_nha_cung_cap = new SelectList(db.nha_Cung_Caps, "Ma_nha_cung_cap", "Ten_nha_cung_cap", san_pham.Ma_nha_cung_cap);
            return View(san_pham);
        }

        // POST: Admin/San_pham/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_san_pham,Ten_san_pham,Ma_danh_muc,Ma_nha_cung_cap,Anh_san_pham,Mo_ta,Gia,Gia_km,Ngay_tao,Trang_thai_1,Trang_thai_2")] San_pham san_pham)
        {
            if (ModelState.IsValid)
            {
                db.Entry(san_pham).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ma_danh_muc = new SelectList(db.Danh_Mucs, "Ma_danh_muc", "Ten_danh_muc", san_pham.Ma_danh_muc);
            ViewBag.Ma_nha_cung_cap = new SelectList(db.nha_Cung_Caps, "Ma_nha_cung_cap", "Ten_nha_cung_cap", san_pham.Ma_nha_cung_cap);
            return View(san_pham);
        }

        // GET: Admin/San_pham/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            San_pham san_pham = db.San_Phams.Find(id);
            if (san_pham == null)
            {
                return HttpNotFound();
            }
            return View(san_pham);
        }

        // POST: Admin/San_pham/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            San_pham san_pham = db.San_Phams.Find(id);
            db.San_Phams.Remove(san_pham);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
