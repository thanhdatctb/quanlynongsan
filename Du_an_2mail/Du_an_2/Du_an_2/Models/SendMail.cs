﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Du_an_2.Models
{
    public class SendMail
    {
        public static void Sendmail(string toEmail, string formEmail, string passEmail, string titleEmail, string ContenEmail)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(toEmail);// gửi đến maill người mua
            mail.From = new MailAddress(formEmail);
            mail.Subject = titleEmail; // Tiêu đề mail mail
            mail.Body = ContenEmail;// nội dung 
            mail.IsBodyHtml = true;// cho phép viết mã Html
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential(formEmail, passEmail);// tài khoản gmail của bạn
            smtp.EnableSsl = true;
            smtp.Send(mail); //gửi
        }

    }
}