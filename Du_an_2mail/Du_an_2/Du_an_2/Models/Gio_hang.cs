﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data2;

using Data2.Models.DataModel;

namespace Du_an_2.Models
{
    public class Gio_hang
    {
        Management db = new Management();
        public int iMa_sp { get; set; }
        public string sTenSp { get; set; }
        public string sHinhanh { get; set; }
        public float fDongia { get; set; }
        public int iSoluong { get; set; }
        public float iThanhtien
        {
            get { return iSoluong * fDongia; }
            set { iThanhtien = value; }
        }
       
        public Gio_hang(int Ma_sp)
        {
            iMa_sp = Ma_sp;
            San_pham sp = db.San_Phams.SingleOrDefault(x => x.Ma_san_pham == iMa_sp);
            sTenSp = sp.Ten_san_pham;
            sHinhanh = sp.Anh_san_pham;
            if (sp.Gia_km<sp.Gia)
            {
                fDongia = sp.Gia_km;
            }
            else
            {
                fDongia = sp.Gia;
            }
            iSoluong = 1;
        }
    }
}