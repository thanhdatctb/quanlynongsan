﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;
using PagedList;

namespace Du_an_2.Controllers
{
    public class San_phamController : Controller
    {
        Management db = new Management();
        // GET: San_pham_moiPartial
        public PartialViewResult San_pham_moiPratial()
        {

           // var san_pham = db.San_Phams.Take(6).ToList();
            //var san_pham = from sp in db.San_Phams.Take(4)
            //             orderby sp.Ma_san_pham descending
            //             select sp;                           

            return PartialView(db.San_Phams.Take(4).OrderByDescending(x=>x.Ma_san_pham).ToList());

        }
        public PartialViewResult San_phamPartial(int? page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 10;
            var san_pham = db.San_Phams.Include(a=>a.Danh_muc).Include(b=>b.Nha_Cung_Cap).Where( x =>x.Trang_thai_1==true && x.Nha_Cung_Cap.Trang_thai_1== true && x.Danh_muc.Trang_thai_1==true).ToList();
            return PartialView(san_pham.ToPagedList(pageNumber, pageSize));
        }
        public PartialViewResult Trai_cayPartial()
        {
            var trai_cay = db.San_Phams.Where(x => x.Ma_danh_muc == 1).ToList();
            return PartialView(trai_cay);
        }
        public PartialViewResult Nuoc_epPartial()
        {
            var nuoc_ep = db.San_Phams.Where(x => x.Ma_danh_muc == 2).ToList();
            return PartialView(nuoc_ep);
        }
        public ViewResult Chi_tiet_san_pham( int id)
        {
           // var sp = db.San_Phams.Include(x => x.Danh_muc);
            var sp = db.San_Phams.Include(x => x.Danh_muc).Include(x=>x.Nha_Cung_Cap).SingleOrDefault(x => x.Ma_san_pham == id);
            if (sp != null)
            {
                return View(sp);
            }
            else
            {
                // trả về trang 404
                Response.StatusCode = 404;
                return null;
            }
        }
      
    }
}