﻿using Du_an_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Controllers
{
    public class Gio_hangController : NotificationController
    {
        Management db = new Management();
        // GET: Gio_hang
        public List<Gio_hang> Lay_gio_hang()
        {
            //khởi tạo session giỏ hàng và ép kiểu bằng 'as'
            List<Gio_hang> lstGio_hang = Session["Gio_hang"] as List<Gio_hang>;
            // nếu giỏ hàng chưa có thì tiến hành khởi tạo
            if (lstGio_hang == null)
            {
                lstGio_hang = new List<Gio_hang>();
                Session["Gio_hang"] = lstGio_hang;
            }
            return lstGio_hang;
        }
        //thêm giỏ hàng
        public ActionResult them_gio_hang(int ma_sp, string strUrl)
        {
            San_pham sp = db.San_Phams.SingleOrDefault(x => x.Ma_san_pham == ma_sp);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                //lấy ra sesion giỏ hàng
                List<Gio_hang> lstGio_hang = Lay_gio_hang();
                //kiểm tra xem trong list class đã có hàng đặt trung chưa
                Gio_hang gh = lstGio_hang.Find(x => x.iMa_sp == ma_sp);
                if (gh == null)
                {
                    gh = new Gio_hang(ma_sp);
                    // thêm sản phẩm vào  list
                    lstGio_hang.Add(gh);

                    return Redirect(strUrl);

                }
                else
                {
                    gh.iSoluong++;

                    return Redirect(strUrl);
                }
            }
        }
        //cập nhật giỏ hàng
        // lấy ra đc số lượng thì dùng Fromcollection
        public ActionResult Cap_nhat_gio_hang(int ma_sp, FormCollection f)
        {
            // kiểm tra mã sản phẩm đúng chưa
            San_pham sp = db.San_Phams.SingleOrDefault(x => x.Ma_san_pham == ma_sp);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                //kiểm tra sản phẩm có tồn tại trong list chưa
                List<Gio_hang> lstGio_hang = Lay_gio_hang();
                Gio_hang san_pham = lstGio_hang.SingleOrDefault(x => x.iMa_sp == ma_sp);
                if (san_pham != null)
                {
                    san_pham.iSoluong = int.Parse(f["txtSo_luong"].ToString());

                }
                return RedirectToAction("Gio_hang");

            }
        }
        //Xóa giỏ hàng
        public ActionResult Xoa_gio_hang(int? ma_sp_x)
        {
            San_pham sp = db.San_Phams.SingleOrDefault(x => x.Ma_san_pham == ma_sp_x);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            else
            {
                List<Gio_hang> lstGio_hang = Lay_gio_hang();
                Gio_hang san_pham = lstGio_hang.SingleOrDefault(x => x.iMa_sp == ma_sp_x);
                if (san_pham != null)
                {
                    lstGio_hang.RemoveAll(x => x.iMa_sp == ma_sp_x);

                }
                if (lstGio_hang.Count == 0)
                {
                    return RedirectToAction("Index", "Home");
                }
                return RedirectToAction("Gio_hang");
            }
        }
        // xây dựng trang giỏ hàng
        public ActionResult Gio_hang()
        {
            if (Session["id"]==null)
            {
                return RedirectToAction("Login", "Khach_hang");
            }
            if (Session["Gio_hang"] == null)
            {
                return RedirectToAction("Index", "Home");

            }
            List<Gio_hang> lstGio_hang = Lay_gio_hang();
            ViewBag.Tongtien = Tongtien();
            return View(lstGio_hang);
        }
        // tính tổng số lượng
        private int Tongsoluong()
        {
            int iTongsoluong = 0;
            List<Gio_hang> lstgiohang = Session["Gio_hang"] as List<Gio_hang>;
            if (lstgiohang != null)
            {
                iTongsoluong = lstgiohang.Sum(x => x.iSoluong);

            }

            return iTongsoluong;
        }
        //tính tổng tiền 
        private float Tongtien()
        {
            float iTongtien = 0;
            List<Gio_hang> lstgiohang = Session["Gio_hang"] as List<Gio_hang>;
            if (lstgiohang != null)
            {
                iTongtien = lstgiohang.Sum(x => x.iThanhtien);
                Session["Tongtien"] = iTongtien;
            }
            return iTongtien;
        }
        // tạo số lượng trên icon giỏ hàng
        public ActionResult Iconpartial()
        {
            if (Tongsoluong() == 0)
            {
                return PartialView();

            }
            ViewBag.Tongsoluong = Tongsoluong();
            ViewBag.Tongtien = Tongtien();
            return PartialView();
        }
        // xây dựng chức năng đặt hàng
        [HttpGet]
        public ActionResult Dat_hang()
        {
            ViewBag.Ma_ptthanh_toan = new SelectList(db.pt_Thanh_Toans, "Ma_pt_thanh_toan", "Ten_thanh_toan");
            ViewBag.Ma_ptvan_chuyen = new SelectList(db.pt_Van_Chuyens, "Ma_van_chuyen", "Ten_van_chuyen");
            return View();
        }

        [HttpPost]
        public ActionResult Dat_hang(Hoa_don Hoa_don)
        {
            var row = "";
            if (Session["id"] == null)
            {
                RedirectToAction("Login", "Khach_hang");
            }

            List<Gio_hang> gh = Lay_gio_hang();
            Khach_hang kh = (Khach_hang)Session["id"];

                var date = DateTime.Now;
             Hoa_don.Ma_khach_hang = kh.Ma_khach_hang;
                Hoa_don.Ngay_tao = date;
                Hoa_don.Trang_thai = false;
                Hoa_don.Trang_thai_1 = true;
                Hoa_don.Tong_tien =Tongtien() ;
                db.Hoa_Dons.Add(Hoa_don);
                db.SaveChanges();
            foreach (var item in gh)
            {
                Hoa_don_ct ct = new Hoa_don_ct();
                ct.Ma_hoa_don = Hoa_don.Ma_hoa_don;
                ct.Ma_san_pham = item.iMa_sp;
                ct.So_luong = item.iSoluong;
                ct.Gia = item.fDongia;
                ct.Gia_km = item.fDongia;
                db.Hoa_Don_Cts.Add(ct);
            }
            db.SaveChanges();

            row += "Cảm ơn bạn đã mua hàng tại shop chúng tôi! <br />";
            row += "Tên người nhận:" + Hoa_don.Ten_khach_hang + "<br />";
            row += "Ngày mua:" + Hoa_don.Ngay_tao + "<br />";
            row += "Tổng tiền:" + Hoa_don.Tong_tien + "<sup>đ</sup><br />";

            SendMail.Sendmail("" + Session["email"] + "", "haphong247@gmail.com", "doanngocdiep123", "Xác nhận đơn hàng từ shop rau sạch ", "" + row + "");
            Show("Đặt hàng thành công", "thanhcong");
            Session["gio_hang"] = null;
            return RedirectToAction("Index", "Home");

        }
    }
}