﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;
using Du_an_2.Models;

namespace Du_an_2.Controllers
{
    public class Khach_hangController : NotificationController
    {
        private Management db = new Management();

        // GET: Khach_hang
        public ActionResult Index()
        {
            var kh = db.Khach_Hangs.ToList();
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        // POST: Khach_hang/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ma_khach_hang,Ho_ten,Email,Gioi_tinh,Ngay_sinh,Dien_thoai,Dia_chi,User,Passwood,Ngay_tao")] Khach_hang khach_hang)
        {
            var tk = db.Khach_Hangs.SingleOrDefault(x => x.User == khach_hang.User);
            if (tk != null)
            {
                Show("Tài khoản này đã có người sử dụng, vui lòng chọn tài khoản khác", "loi");
                return View();
            }
            else
            {
                var tks = db.Khach_Hangs.SingleOrDefault(x => x.Email == khach_hang.Email);
                if (tks != null)
                {
                    Show("Email này đã có người sử dụng, vui lòng chọn tài khoản khác", "loi");
                    return View();
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        khach_hang.Ngay_tao = DateTime.Now;
                        db.Khach_Hangs.Add(khach_hang);
                        db.SaveChanges();
                        Show("Chúc mừng bạn đã đăng lý thành công", "thanhcong");
                        return Redirect("~/Khach_hang/Login");
                    }
                }
            }


            return View(khach_hang);
        }

        // GET: Khach_hang/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["id"] == null)
            {
                Show("Bạn cần đăng nhập trước khi làm tác vụ này", "canhbao");
                return RedirectToAction("Login");
            }
            else
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Khach_hang khach_hang = db.Khach_Hangs.Find(id);
                if (khach_hang == null)
                {
                    return HttpNotFound();
                }

                return View(khach_hang);
            }
        }

        // POST: Khach_hang/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ma_khach_hang,Ho_ten,Email,Gioi_tinh,Ngay_sinh,Dien_thoai,Dia_chi,User,Passwood,Ngay_tao")] Khach_hang khach_hang)
        {
            if (Session["id"] == null)
            {
                Show("Bạn cần đăng nhập trước khi làm tác vụ này", "canhbao");
                return RedirectToAction("Login");
            }
            else
            {
                if (!ModelState.IsValid)
                {

                    db.Entry(khach_hang).State = EntityState.Modified;
                    db.SaveChanges();
                    Show("Sủa thông tin thành công", "thanhcong");
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(khach_hang);
        }




        //Đăng nhập cho khách hàng
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string user, string pass)
        {
            var account = db.Khach_Hangs.SingleOrDefault(x => x.User == user && x.Passwood == pass);
            if (account != null)
            {
                Session["id"] = account;
                Session["userid"] = account.Ma_khach_hang;
                Session["user"] = account.User;
                Session["fullname"] = account.Ho_ten;
                Session["dienthoai"] = account.Dien_thoai;
                Session["email"] = account.Email;
                Session["diachi"] = account.Dia_chi;
                return RedirectToAction("Index", "Home");
                //return RedirectToAction("Index");
            }
            else
            {
                TempData["msg"] = "<div class='alert-danger'> Tên tài khoản hoặc mật khẩu không đúng</div>";
                return View();
            }

        }
        public ActionResult Logout()
        {
            Session["userid"] = null;
            Session["fullname"] = null;
            Session["Tongtien"] = null;
            Session["dienthoai"] = null;
            Session["email"] = null;
            Session["diachi"] = null;
            Session["id"] = null;
            return RedirectToAction("Index", "Home");
        }
        //Lịch sử giao dịch cho khách hàng
        public ActionResult History_Transaction(int? id)
        {

            if (Session["id"] == null)
            {
                Show("Bạn cần đăng nhập trước khi xem thông tin này", "canhbao");
                return RedirectToAction("Login", "Khach_hang");
            }

            var ls = db.Hoa_Dons.Include(h => h.Khach_hang).Include(h => h.Pt_Thanh_Toan).Include(a => a.Hoa_Don_Ct).Include(h => h.Pt_Van_Chuyen).Where(x => x.Ma_khach_hang == id).OrderByDescending(x => x.Ma_hoa_don);
            return View(ls.ToList());
        }

        //Phản hồi khách hàng
        public ActionResult PhanHoiKH(int? id)
        {

            if (Session["id"] == null)
            {
                Show("Bạn cần đăng nhập trước khi xem thông tin này", "canhbao");
                return RedirectToAction("Login", "Khach_hang");
            }

            var ls = db.phan_Hoi_Khs.Include(h => h.Khach_Hang).Where(x => x.Ma_khach_hang == id).OrderByDescending(x => x.Ma_phan_hoi);
            return View(ls);
        }
        //chi tiết lịch sử từng đơn hàng
        public ActionResult Transaction_details(int? id)
        {

            var ct = db.Hoa_Don_Cts.Include(x => x.Hoa_don).Where(a => a.Ma_hoa_don == id).ToList();
            return View(ct);
        }
        // chức năng lấy lại mật khẩu cho khách hàng
        public ActionResult Quen_pass()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Quen_pass(string user, string email)
        {
            var acc = db.Khach_Hangs.FirstOrDefault(x => x.Email == email && x.User == user);
            if (acc == null)
            {
                Show("Tên tài khoản hoặc Email không đúng.Xin thử lại!!", "loi");
                return RedirectToAction("Quen_pass", "Khach_hang");
            }
            else
            {
                var mk = acc.Passwood;
                SendMail.Sendmail("" + email + "", "haphong247@gmail.com", "doanngocdiep123", "Xác nhận mật khẩu từ shop rau củ quả", "Mật khẩu của bạn là" + mk + "");
                Show("Mật khẩu của bạn đã được gửi vào email. Vui lòng vào email để xác nhận!!", "thanhcong");

                return RedirectToAction("Login", "Khach_hang");
            }


        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}