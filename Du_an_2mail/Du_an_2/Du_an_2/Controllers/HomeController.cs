﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data2;


namespace Du_an_2.Controllers
{

    public class HomeController : Controller
    {
        Management db = new Management();
        public ActionResult Index()
        {

            return View();
        }
        public PartialViewResult Banner()
        {
            var banner = db.Banners.Take(3).ToList();
            return PartialView(banner);
        }
    }
}