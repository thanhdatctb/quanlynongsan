﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Du_an_2.Controllers
{
    public class NotificationController : Controller
    {
        // GET: Notification
        protected void Show(string message, string type)
        {
            TempData["AlertMessage"] = message;
            if (type == "thanhcong")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "loi")
            {
                TempData["AlertType"] = "alert-danger";
            }
            else if (type == "canhbao")
            {
                TempData["AlertType"] = "alert-warning";
            }
        }
    }
}