﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;
using PagedList;

namespace Du_an_2.Controllers
{
    public class Nha_cung_capController : Controller
    {
        // GET: Nha_cung_cap
        Management db = new Management();
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult Nha_cung_capPartial()
        {
            var ncc = db.nha_Cung_Caps.Where(x => x.Trang_thai_1 == true).ToList();
            return PartialView(ncc);
        }
        public ViewResult San_pham_ncc(int? id, int? page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 5;
            Nha_cung_cap ncc = db.nha_Cung_Caps.SingleOrDefault(x => x.Ma_nha_cung_cap == id);
            if (ncc == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<San_pham> sp = db.San_Phams.Where(x => x.Ma_nha_cung_cap == id && x.Trang_thai_1==true).ToList();
            return View(sp.ToPagedList(pageNumber, pageSize));

        }
      
    }
}