﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data2;
using PagedList;

namespace Du_an_2.Controllers
{
    public class Tim_kiemController : Controller
    {
        // GET: Tim_kiem
        Management db = new Management();
        public ActionResult Ket_qua_tim_kiem( string ten,int? page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 10;
            var tk = db.San_Phams.Where(x => x.Ten_san_pham.Equals(ten)).ToList();
            return View(tk.ToPagedList(pageNumber, pageSize));
        }
    }
}