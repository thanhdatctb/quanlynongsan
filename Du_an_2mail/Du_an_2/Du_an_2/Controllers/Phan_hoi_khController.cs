﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;

namespace Du_an_2.Controllers
{
    public class Phan_hoi_khController : Controller
    {
        private Management db = new Management();

        // GET: Phan_hoi_kh
        public ActionResult Index()
        {
            var phan_Hoi_Khs = db.phan_Hoi_Khs.Include(p => p.Khach_Hang);
            return View(phan_Hoi_Khs.ToList());
        }

        // GET: Phan_hoi_kh/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phan_hoi_kh phan_hoi_kh = db.phan_Hoi_Khs.Find(id);
            if (phan_hoi_kh == null)
            {
                return HttpNotFound();
            }
            return View(phan_hoi_kh);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
