﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data2;
using Data2.Models.DataModel;
using PagedList;

namespace Du_an_2.Controllers
{
    public class Danh_mucController : Controller
    {
        // GET: Danh_muc
        Management db = new Management();

        public PartialViewResult Danh_mucPartial()
        {
            var danh_muc = db.Danh_Mucs.Where(x => x.Trang_thai_1 == true).ToList();
            return PartialView(danh_muc);
        }
        //  hiển thị sản phẩm theo danh mục
        public ViewResult San_pham_theo_dm(int? id, int? page)
        {
            //kiểm tra xem mã danh mục có hay không
            int pageNumber = (page ?? 1);
            int pageSize = 5;
            Danh_muc dm = db.Danh_Mucs.SingleOrDefault(x => x.Ma_danh_muc == id);
            if (dm == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            List<San_pham> san_pham = db.San_Phams.Where(x => x.Ma_danh_muc == id && x.Trang_thai_1 == true).ToList();
            return View(san_pham.ToPagedList(pageNumber, pageSize));
        }

    }
}