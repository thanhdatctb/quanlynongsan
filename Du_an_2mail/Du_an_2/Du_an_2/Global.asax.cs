﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Du_an_2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start()
        {

            Session["userid"] = null;
            Session["fullname"] = null;
            Session["Tongtien"] = null;
            Session["dienthoai"] = null;
            Session["email"] = null;
            Session["diachi"] = null;
            Session["id"] = null;
            // admin
            Session["iduser"] = null;
            Session["name"] = null;
            Session["username"] = null;
            Session["password"] = null;
            
        }
    }

}

